import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Basics from "../views/Basics.vue";
import Players from "../views/Players.vue";
import Games from "../views/Games.vue";
import Scores from "../views/Scores.vue";
import Impressum from "../views/Impressum.vue";
import Datenschutz from "../views/Datenschutz.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/basics",
    name: "Basics",
    component: Basics
  },
  {
    path: "/players",
    name: "Players",
    component: Players
  },
  {
    path: "/games",
    name: "Games",
    component: Games
  },
  {
    path: "/scores",
    name: "Scores",
    component: Scores
  },

  {
    path: "/impressum",
    name: "Impressum",
    component: Impressum
  },

  {
    path: "/datenschutz",
    name: "Datenschutz",
    component: Datenschutz
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
