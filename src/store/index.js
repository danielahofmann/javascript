import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    playerList:[
      {playername: "6ix9ine", firstname: "Hector", lastname: "Snitch",city:"New York"},
      {playername: "Max420", firstname: "Max", lastname: "Petersen",city:"Berlin"},
      {playername: "theLegend27", firstname: "Peter", lastname: "Mustermann",city:"Bremen"},
    ],
    cityList: ["Jena", "Leipzig", "Berlin"],
  },
  mutations: {
    INCREASE_COUNTER(state){
      state.counter++;
    },
    DECREASE_COUNTER(state){
      state.counter--;
    }
  }, 
  actions: {
    inc(context){
      context.commit("INCREASE_COUNTER");
    },
    dec(context){
      context.commit("DECREASE_COUNTER");
    }
  },
  modules: {}
});
